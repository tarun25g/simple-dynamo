package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.Serializable;

/**
 * Created by tarun on 4/18/15.
 */
public class Packet implements Serializable {
    private PacketType pType;
    private String senderPort;
    private String receiverPort;
    private String key;
    private String val;
    private String keyHash;
    private String packetInitiator;

    public String getPacketInitiator() {
        return packetInitiator;
    }
    public void setPacketInitiator(String packetInitiator) {
        this.packetInitiator = packetInitiator;
    }

    Packet() {}
    Packet(PacketType pType) { this.pType=pType; }

    public PacketType getpType() {
        return pType;
    }
    public void setpType(PacketType pType) {
        this.pType = pType;
    }

    public String getSenderPort() {
        return senderPort;
    }
    public void setSenderPort(String senderPort) {
        this.senderPort = senderPort;
    }

    public String getReceiverPort() {
        return receiverPort;
    }
    public void setReceiverPort(String receiverPort) {
        this.receiverPort = receiverPort;
    }

    public String getVal() {
        return val;
    }
    public void setVal(String val) {
        this.val = val;
    }

    public String getKeyHash() {
        return keyHash;
    }
    public void setKeyHash(String keyHash) {
        this.keyHash = keyHash;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
}

enum PacketType {
    INSERT_REQ, REBALANCE_SELF_REQ, REBALANCE_SELF_ACK, REBALANCE_OTH_REQ, REBALANCE_OTH_ACK, INSERT_ACK,INSERT_REPLICA_REQ, INSERT_REPLICA_ACK, DELETE_REQ, DELETE_REPLICA_REQ, DELETE_LOCAL_REQ, QUERY_REQ, QUERY_REQ_ACK, QUERY_LOCAL_REQ, QUERY_LOCAL_ACK, QUERY_REQ_REP1, QUERY_REQ_REP2, QUERY_REQ_REPL1_ACK, QUERY_REQ_REPL2_ACK
}