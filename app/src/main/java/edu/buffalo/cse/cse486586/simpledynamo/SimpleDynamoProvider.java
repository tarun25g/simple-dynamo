package edu.buffalo.cse.cse486586.simpledynamo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;

public class SimpleDynamoProvider extends ContentProvider {
    ConcurrentHashMap<String, String> localData = new ConcurrentHashMap<>();
    String configFileName = "OmNamoNarayana";
    Boolean isRecoveringFromFailure = false;
    public String remotePort[] = {"11108", "11112", "11116", "11120", "11124"};
    static final int SERVER_PORT = 10000;
    Boolean isInsertAck = false;
    Node myNode = new Node();
    Boolean allAckReceived = false;
    Boolean isSingleQueryAnswered = false;
    Boolean isSingleQueryRepl1Answered = false;
    Boolean isSingleQueryRepl2Answered = false;
    Boolean fileLock = false;

    String globalQueryResp = "";
    String singleQueryResp = "";
    String singleQueryRespRepl1 = "";
    String singleQueryRespRepl2 = "";

    private List<Node> ringNodes = new ArrayList<>();

    /* Deletion */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (selection.equals("@")) {
            // delete local files
            return deleteFilesLocally();
        }
        if (selection.equals("*")) {
            for (String port : remotePort) {
                new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_LOCAL_REQ", port);
            }
        }
        else {
            String key = selection;

            String keySHA="";
            try {
                keySHA = genHash(key);
            } catch (NoSuchAlgorithmException e) {
                System.out.println(e.getMessage());
            }
            RingComparator keyComp = new RingComparator();

            for (int i = 0; i < ringNodes.size(); i++) {
                Node sNode = ringNodes.get(i);
                if ((keySHA.equals(sNode.getPortSHA())) ||
                        (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && sNode.isFirstNode())||
                        (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1 && sNode.isFirstNode()) ||
                        (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorPortSHA() )==1))) {
                    System.out.println("Key " + key + " with SHA " + keySHA + " to be inserted at " + sNode.getPort() + " with SHA " + sNode.getPortSHA() + " with successor SHA as " + sNode.getSuccessorPortSHA() + " and Predecessor SHA as " +  sNode.getPredecessorPortSHA());

                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REQ", sNode.getPort(), key);
                    //new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REPLICA_REQ", sNode.getPredecessorPort(), key);
                    //if (i!=0) new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REPLICA_REQ", ringNodes.get((i-1)).getPredecessorPort(), key);
                    //else new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REPLICA_REQ", ringNodes.get((4)%ringNodes.size()).getPredecessorPort(), key);
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REPLICA_REQ", sNode.getSuccessorPort(), key);
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "DELETE_REPLICA_REQ", ringNodes.get((i+1)%ringNodes.size()).getSuccessorPort(), key);

                    break;
                }
            }
        }
        return 0;
    }
    int deleteFilesLocally () {

        localData.clear();
        File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
        File[] allFiles = baseDir.listFiles();

        for (File f : allFiles) {
            if (!f.delete()) return -1;
        }
        return 0;
    }
    /* Deletion ENDS */

    String getValueFromLocalDataDict (String key) {
        if (localData.containsKey(key)) {
            String out = localData.get(key);
            return out.split("@")[0];
        }
        else return null;
    }
    String getVersionFromLocalDataDict (String key) {
        if (localData.containsKey(key)) {
            String out = localData.get(key);
            return out.split("@")[1];
        }
        else return null;
    }

    void putInLocalDataDict (String key, String value) {
        if (key != null && value != null && !key.equals("") && !value.equals(""))
            if (localData.containsKey(key)) {
                String out = localData.get(key);
                int version = Integer.valueOf(out.split("@")[1]);

                String newVal = value + "@" + Integer.toString(++version);
                localData.put(key, newVal);
            } else {
                String newVal = value + "@1";
                localData.put(key, newVal);
            }
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    /* Insertion */
    @Override
    public synchronized Uri insert(Uri uri, ContentValues values) {
        String key = values.getAsString("key");
        String value = values.getAsString("value");
        String keySHA="";
        try {
            keySHA = genHash(key);
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
        RingComparator keyComp = new RingComparator();

        for (int i = 0; i < ringNodes.size(); i++) {
            Node sNode = ringNodes.get(i);
            if ((keySHA.equals(sNode.getPortSHA())) ||
                    (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && sNode.isFirstNode())||
                    (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1 && sNode.isFirstNode()) ||
                    (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorPortSHA() )==1))) {
                System.out.println("Key " + key + " with SHA " + keySHA + " to be inserted at " + sNode.getPort() + " with SHA " + sNode.getPortSHA() + " with successor SHA as " + sNode.getSuccessorPortSHA() + " and Predecessor SHA as " + sNode.getPredecessorPortSHA());

                Socket socket;

                Packet pack = new Packet();
                pack.setpType(PacketType.INSERT_REQ);
                pack.setSenderPort(myNode.getPort());
                pack.setReceiverPort(sNode.getPort());
                pack.setKey(key);
                pack.setVal(value);
                try {
                    //new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REQ", sNode.getPort(), key, value);

                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.valueOf(sNode.getPort()));

                    System.out.println("CLIENT: First Packet Sent.");

                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(pack);
                    //ost.flush();
                    //ost.close();
                    //socket.close();

                    //TimeUnit.MILLISECONDS.sleep(1000);
                } catch (Exception e) {
                    System.out.println("*************Port was " + sNode.getPort() +" and send effort was unsuccessful so Sending Replica Now************* at "+sNode.getSuccessorPort());
                    System.out.println("*************Port was " + sNode.getPort() +" and send effort was unsuccessful so Sending Replica Now************* at "+ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort());
                    System.out.println("*************Port was " + sNode.getPort() +" and send effort was unsuccessful so Sending Replica Now*************");
                    //pack.setpType(PacketType.INSERT_REPLICA_REQ);
                    //sendMessage(sNode.getSuccessorPort(), pack);
                    //sendMessage(ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort(), pack);
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REPLICA_REQ", sNode.getSuccessorPort(), key, value);
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REPLICA_REQ", ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort(), key, value);
                }
                /*
                if (!isInsertAck) {
                    System.out.println("**Sending Replica Now");
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REPLICA_REQ", sNode.getSuccessorPort(), key, value);
                    new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REPLICA_REQ", ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort(), key, value);
                    isInsertAck = false;
                } else {
                    System.out.println("INSERT_ACK_RECEIVED ON TIME.. AWESOME..");
                }
                */

                break;
            }
        }
        return null;
    }
    void insertLocally (String key, String value) {
        try {
            putInLocalDataDict(key, value);
            while (fileLock);
            if (!fileLock) fileLock=true;
            FileOutputStream outputStream = getContext().openFileOutput(key, Context.MODE_PRIVATE);
            outputStream.write(value.getBytes());
            System.out.println("Inserting Data Locally........" + value);
            outputStream.close();
            fileLock=false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /* Insertion ENDS */

    /* Failure Recovery */
    void createStartupConfig () {
        try {
            String key = configFileName;
            String value = "1";         // This should be config data
            FileOutputStream outputStream = getContext().openFileOutput(key, Context.MODE_PRIVATE);
            outputStream.write(value.getBytes());
            //System.out.println("Inserting Data Locally........" + value);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    Boolean ifConfExists () {
        try {
            File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
            File f =  new File(baseDir.getAbsolutePath().toString() +"/"+ configFileName);
            return f.exists();
        } catch (Exception e) {
            return false;
        }
    }
    /* Failure Recovery Ends */

    @Override
    public boolean onCreate() {
        TelephonyManager tel = (TelephonyManager) this.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);
        String myPort = String.valueOf((Integer.parseInt(portStr) * 2));
        myNode.setPort(myPort);

        /* Detect Failure */
        if (!ifConfExists()) createStartupConfig();
        else isRecoveringFromFailure = true;

        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);
        } catch (IOException e) {
            //Log.e(TAG, "Can't create a ServerSocket");
            System.out.println("Can't create a ServerSocket");
        }

        try {
            String myPortSHA = genHash(Integer.toString(Integer.valueOf(myPort) / 2));
            myNode.setPortSHA(myPortSHA);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        contructRingOfNodes();

        //TEST... printing the entire ring for test
        for (int i = 0; i < ringNodes.size(); i++) {
            System.out.println("Nodes in ring are- " + ringNodes.get(i).getPort() + "-" + ringNodes.get(i).getSuccessorPort() +"-" + ringNodes.get(i).getSuccessorPortSHA() + "-" + ringNodes.get(i).getPredecessorPort() + "-" + ringNodes.get(i).isFirstNode().toString());
            if (ringNodes.get(i).getPort().equals(myNode.getPort())) {
                myNode.setSuccessorPort(ringNodes.get(i).getSuccessorPort());
                myNode.setPredecessorPort(ringNodes.get(i).getPredecessorPort());
            }
        }

        if (isRecoveringFromFailure) {
            System.out.println("Now rebalancing");
            System.out.println ("My details are - " + myNode.getPort() + "->" + myNode.getPortSHA() + "-" + myNode.getSuccessorPort() + "-" + myNode.getPredecessorPort());
            new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "REBALANCE_REQ");

            //rebalanceData();
            isRecoveringFromFailure = false;
        }

        System.out.println("My first node status is-" + myNode.isFirstNode().toString());

        return false;
    }

    void contructRingOfNodes() {
        for (String port : remotePort) {
            String portSHA = "";
            try {
                portSHA = genHash(Integer.toString(Integer.valueOf(port) / 2));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            Node n = new Node();
            n.setPort(port);
            n.setPortSHA(portSHA);
            ringNodes.add(n);
        }
        Collections.sort(ringNodes, new RingComparator());
        int numPorts = ringNodes.size();
        for (int i = 0; i < numPorts; i++) {
            ringNodes.get(i).setSuccessorPort(ringNodes.get((i + 1) % numPorts).getPort());
            ringNodes.get(i).setSuccessorPortSHA(ringNodes.get((i + 1) % numPorts).getPortSHA());
            if (i != 0) {
                ringNodes.get(i).setPredecessorPort(ringNodes.get((i - 1) % numPorts).getPort());
                ringNodes.get(i).setPredecessorPortSHA(ringNodes.get((i - 1) % numPorts).getPortSHA());
            }
            else {
                ringNodes.get(i).setPredecessorPort(ringNodes.get((numPorts - 1) % numPorts).getPort());
                ringNodes.get(i).setPredecessorPortSHA(ringNodes.get((numPorts - 1) % numPorts).getPortSHA());
            }
            // set all first node status as 'false' initially
            if (i!=0) ringNodes.get(i).setIsFirstNode(false);
            else ringNodes.get(i).setIsFirstNode(true);
        }
        if (ringNodes.get(0).getPort().equals(myNode.getPort())) myNode.setIsFirstNode(true);
        else myNode.setIsFirstNode(false);
    }

    void rebalanceData() {
        /* Node rebalances data - Takes Data of 2 predecessors and 1 sucessor */
        System.out.println ("In rebalance data function");

        Packet pk = new Packet(PacketType.REBALANCE_OTH_REQ);

        pk.setSenderPort(myNode.getPort());

        sendMessage(myNode.getPredecessorPort(), pk);

        int i;
        for (i = 0; i < ringNodes.size(); i++) if (ringNodes.get(i).getPort().equals(myNode.getPort())) break;
        //System.out.println("My Node IP is " + myNode.getPort() + " and my index is " + i);
        if (i != 0) sendMessage(ringNodes.get(i-1).getPredecessorPort(), pk);
        else sendMessage(ringNodes.get(ringNodes.size()-1).getPredecessorPort(), pk);

        System.out.println ("Sending rebalance self request to-"+myNode.getSuccessorPort());
        pk.setpType(PacketType.REBALANCE_SELF_REQ);
        sendMessage(myNode.getSuccessorPort(), pk);
    }

    class RingComparator implements Comparator<Node> {
        public int compare(Node nd1, Node nd2) {
            if (nd1.getPortSHA().compareTo(nd2.getPortSHA()) > 0) return 1;
            else return -1;
        }

        public int compare(String keySHA1, String keySHA2) {
            if (keySHA1.compareTo(keySHA2) > 0) return 1;
            else return -1;
        }
    }

    String queryLocally(String key) {          // changed
        FileInputStream fstream;
        BufferedReader buffr;
        String line;
        if (key!=null && !key.equals("")) {
            try {
                /*
                StringBuffer stringBuffer = new StringBuffer();
                fstream = getContext().openFileInput(key);
                buffr = new BufferedReader(new InputStreamReader(fstream));

                while ((line = buffr.readLine()) != null) {
                    stringBuffer.append(line);
                }
                buffr.close();
                //cur.addRow(new Object[]{selection, stringBuffer.toString()});
                */
                if (localData.containsKey(key)) {
                    String val = localData.get(key);
                    return key+"@"+val;
                }

                else return null;

                //return key+"@"+stringBuffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String response="";
        File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledynamo/files");
        File[] allFiles = baseDir.listFiles();
        System.out.println("Executing Local Dump QUery");
        Iterator it = localData.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            response += pair.getKey()+"@"+pair.getValue()+"&";
        }

        /*
        //System.out.println ("Just came here 0");
        while (fileLock);
        if (!fileLock) fileLock=true;

        if (allFiles != null && allFiles.length > 0)
            for (File f : allFiles) {
                if (f.getName().equals(configFileName)) { continue; }
                StringBuffer stringBuffer = new StringBuffer();
                fstream = getContext().openFileInput(f.getName());
                buffr = new BufferedReader(new InputStreamReader(fstream));
                while ((line = buffr.readLine()) != null) {
                    stringBuffer.append(line);
                }
                response += f.getName()+"@"+stringBuffer.toString()+"&";
                //System.out.println ("Retrieved " + f.getName()+ "\t--\t" +stringBuffer.toString() );
                buffr.close();
            }
        fileLock=false;
        */
        //System.out.println ("Just came here 1");
        return response;
    }

    @Override
    public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String[] columnNames = {"key", "value"};
        MatrixCursor cur = new MatrixCursor(columnNames);
        System.out.println("Query is " + selection);
        String line;
        String key = selection;
        System.out.println("Received a "+ key);

        if (key.equals("@") || key.equals("\"@\"")) {
            String response = queryLocally("");

            String[] allItems = response.split("&");
            if (allItems != null && allItems.length>0)
                for (String item: allItems) {
                    //System.out.println("Item is " + item);
                    String[] key_val = item.split("@");
                    if (key_val!=null && key_val.length>1 && !key_val[0].equals("") && !key_val[1].equals("")) {
                        //System.out.println("Key is " + key_val[0] + " and value is " + key_val[1]);
                        cur.addRow(new Object[]{key_val[0], key_val[1]});
                    }
                }
            return cur;
        }
        else if (key.equals("*") || key.equals("\"*\"")) {
            // global collection
            for (int i=0; i<ringNodes.size(); i++) {
                if (ringNodes.get(i).getPort().equals(myNode.getPort())) {
                       globalQueryResp += queryLocally("");
                }
                else {
                    System.out.println("Sending local Query request to " + ringNodes.get(i).getPort());
                    //System.out.println("CLIENT: Query Packet Sending 1.");

                    Packet pack = new Packet(PacketType.QUERY_LOCAL_REQ);
                    pack.setReceiverPort(ringNodes.get(i).getPort());
                    pack.setSenderPort(myNode.getPort());
                    pack.setKey(key);
                    //System.out.println("CLIENT: Query Packet Sending 2.");
                    sendMessage(ringNodes.get(i).getPort(), pack);
                    //System.out.println("CLIENT: Query Packet Sent.");
                }
            }
            System.out.println("Sent all requests");

            try { TimeUnit.MILLISECONDS.sleep(1500); } catch (Exception e) {System.out.println("Do nothing");}
            //while (!allAckReceived);
            System.out.println("Global Query Responses Received from all");

            HashMap<String, String> globalData = new HashMap<>();

            System.out.println("*************Global Query is "+globalQueryResp);
            String[] allItems = globalQueryResp.split("&");
            int initcount = 0;
            if (allItems != null && allItems.length>0)
                for (String item: allItems) {
                    // System.out.println("Item is " + item);
                    initcount++;
                    String[] key_val = item.split("@");
                    if (key_val!=null && key_val.length>0) {// && !key_val[0].equals("") && !key_val[1].equals("")) {
                        System.out.println("Key is " + key_val[0] + " and value is " + key_val[1]);
                        if (!globalData.containsKey(key_val[0]))
                            globalData.put(key_val[0], key_val[1]+"@"+key_val[2]);
                        else if (Integer.valueOf(globalData.get(key_val[0]).split("@")[1]) < Integer.valueOf(key_val[2])) {
                                globalData.put(key_val[0], key_val[1] + "@" + key_val[2]);
                        }        //cur.addRow(new Object[]{key_val[0], key_val[1]});
                    }
                }

            Iterator it = globalData.entrySet().iterator();
            int count = 0;
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                count++;
                String val = (String)pair.getValue();
                System.out.println ("Output for Global Query is "+ val.split("@")[0]);

                cur.addRow(new Object[]{pair.getKey(), val.split("@")[0]});
            }
            System.out.println("*************************************Count is ************************************************"+count+" and initcount is ****"+initcount);
            System.out.println("All ack received and tested");
            globalQueryResp="";
            allAckReceived=false;
            return cur;
        }
        else {
            String keySHA="";
            try {
                keySHA = genHash(key);
            } catch (NoSuchAlgorithmException e) {
                System.out.println(e.getMessage());
            }
            RingComparator keyComp = new RingComparator();

            int i = 0;
            for (i = 0; i < ringNodes.size(); i++) {
                Node sNode = ringNodes.get(i);
                if ((keySHA.equals(sNode.getPortSHA())) ||
                        (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && sNode.isFirstNode()) ||
                        (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1 && sNode.isFirstNode()) ||
                        (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1))) {
                    System.out.println("Key " + key + " to be queried at " + sNode.getPort() + " with successor as " + sNode.getSuccessorPort() + " and further successor at " + ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort());

                    //new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "QUERY_REQ", sNode.getPort(), key);
                    Packet qPack = new Packet(PacketType.QUERY_REQ);
                    qPack.setSenderPort(myNode.getPort());
                    qPack.setReceiverPort(sNode.getPort());
                    qPack.setKey(key);
                    if (sNode.getPort().equals(myNode.getPort())) {
                        singleQueryResp = queryLocally(key);
                    }
                    else {
                        try {
                            Socket sock1 = sendMessage(sNode.getPort(), qPack, true);
                            ObjectInputStream in = new ObjectInputStream(sock1.getInputStream());
                            System.out.println("Reading Packet");
                            Packet pack = (Packet) in.readObject();
                            singleQueryResp = pack.getVal();
                            sock1.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.println("Query for key " + key + " at node " + sNode.getPort());
                    }
                    System.out.println("Response is "+singleQueryResp);

                    if (sNode.getSuccessorPort().equals(myNode.getPort())) {
                        singleQueryRespRepl1 = queryLocally(key);
                    }
                    else {
                        qPack.setpType(PacketType.QUERY_REQ_REP1);
                        qPack.setReceiverPort(sNode.getSuccessorPort());
                        System.out.println("Query for key " + key + " at node " + sNode.getSuccessorPort());
                        try {
                            Socket sock2 = sendMessage(sNode.getSuccessorPort(), qPack, true);
                            ObjectInputStream in = new ObjectInputStream(sock2.getInputStream());
                            System.out.println("Reading Packet");
                            Packet pack = (Packet) in.readObject();
                            sock2.close();
                            singleQueryRespRepl1 = pack.getVal();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println ("i is " + i);
                    if (ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort().equals(myNode.getPort())) {
                        singleQueryRespRepl2 = queryLocally(key);
                    }
                    else {
                        qPack.setpType(PacketType.QUERY_REQ_REP2);
                        qPack.setReceiverPort(ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort());
                        System.out.println("Query for key " + key + " at node " + ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort());
                        try {
                            Socket sock3 = sendMessage(ringNodes.get((i + 1) % ringNodes.size()).getSuccessorPort(), qPack, true);
                            ObjectInputStream in = new ObjectInputStream(sock3.getInputStream());
                            System.out.println("Reading Packet");
                            Packet pack = (Packet) in.readObject();
                            sock3.close();
                            singleQueryRespRepl2 = pack.getVal();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
            //String[] allItems = singleQueryResp.split("&");
            System.out.println("All things distributed.. All items returned -" + singleQueryResp + " and " + singleQueryRespRepl1 + " and " + singleQueryRespRepl2);

            //try { TimeUnit.MILLISECONDS.sleep(500); } catch (Exception e) {System.out.println("Do nothing");}

            String[] key_val=null, key_val_repl1=null, key_val_repl2=null;

            if (singleQueryResp!=null && !singleQueryResp.equals("")) {
                key_val = singleQueryResp.split("@");
            }

            if (singleQueryRespRepl1!=null && !singleQueryRespRepl1.equals("")) {
                key_val_repl1 = singleQueryRespRepl1.split("@");
            }
            if (singleQueryRespRepl2!=null && !singleQueryRespRepl2.equals("")) {
                key_val_repl2 = singleQueryRespRepl2.split("@");
            }

            System.out.println("Following replies Received - " + singleQueryResp + " and " + singleQueryRespRepl1 + " and " + singleQueryRespRepl2);

            // find best version ******************************************************
            String finalVal = findMaxVersionValue(key_val, key_val_repl1, key_val_repl2);
            cur.addRow(new Object[]{key, finalVal});
            //cur.addRow(new Object[]{key_val[0], key_val[1]});

            System.out.println("All ack received and tested");
            singleQueryResp = "";
            isSingleQueryAnswered = false;
            singleQueryRespRepl1 = "";
            isSingleQueryRepl1Answered = false;
            singleQueryRespRepl2 = "";
            isSingleQueryRepl2Answered = false;
            return cur;
        }
    }

    String findMaxVersionValue (String[] key_val1, String[] key_val2, String[] key_val3 ) {
        int ver1=0, ver2=0, ver3=0;
        if (key_val1!=null) ver1 = Integer.valueOf(key_val1[2]);
        if (key_val2!=null) ver2 = Integer.valueOf(key_val2[2]);
        if (key_val3!=null) ver3 = Integer.valueOf(key_val3[2]);


        if (ver1>=ver2 && ver1>=ver3 && ver1>0) return key_val1[1];
        if (ver2>=ver1 && ver2>=ver3 && ver2>0) return key_val2[1];
        if (ver3>=ver1 && ver3>=ver2 && ver3>0) return key_val3[1];

        if (key_val1[1]!=null) return key_val1[1];
        if (key_val2[1]!=null) return key_val2[1];
        if (key_val3[1]!=null) return key_val3[1];

        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private String genHash(String input) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sha1Hash = sha1.digest(input.getBytes());
        Formatter formatter = new Formatter();
        for (byte b : sha1Hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    private class ServerTask extends AsyncTask<ServerSocket, String, Void> {
        @Override
        protected Void doInBackground(ServerSocket... sockets) {
            ServerSocket serverSocket = sockets[0];
            Socket ss;
            int numResponsesForGlobalQuery = 0;
            ObjectInputStream in;
            try {
                while (true) {
                    System.out.println("Server: Started..");
                    ss = serverSocket.accept();
                    in = new ObjectInputStream(ss.getInputStream());

                    System.out.println("Reading Packet");

                    Packet pack = (Packet) in.readObject();

                    System.out.println("Received Packet Request type is -" + pack.getpType());
                    //if (pack.getpType() == PacketType.QUERY_REQ) {
                    //String test = "";
                    //try {test = genHash(pack.getKey());} catch (Exception e){}
                    //System.out.println("Query is for " + pack.getKey());// + " - " + test);
                    //}
                    //in.close();

                    if (pack.getpType() == PacketType.INSERT_REQ) {
                        // Insert Packet Locally
                        System.out.println("Packet Insert Request Type - " + pack.getpType());
                        insertLocally(pack.getKey(), pack.getVal());
                        // send ack
                        pack.setpType(PacketType.INSERT_REPLICA_REQ);
                        pack.setPacketInitiator(pack.getSenderPort());
                        String ackReceiver = pack.getSenderPort();

                        pack.setSenderPort(myNode.getPort());
                        System.out.println("Next port is " +myNode.getSuccessorPort());
                        sendMessage(myNode.getSuccessorPort(), pack);

                        int i;
                        for (i = 0; i < ringNodes.size(); i++) if (ringNodes.get(i).getPort().equals(myNode.getPort())) break;

                        System.out.println("My Node IP is " + myNode.getPort() + " and my index is " + i);
                        sendMessage(ringNodes.get((i+1)%ringNodes.size()).getSuccessorPort(), pack);

                        // sending ack to client
                        //pack.setpType(PacketType.INSERT_ACK);
                        //sendMessage(ackReceiver, pack);
                        continue;
                    }

                    if (pack.getpType() == PacketType.INSERT_REPLICA_REQ) {
                        // Insert Packet Locally
                        System.out.println("Packet Type - " + pack.getpType());
                        insertLocally(pack.getKey(), pack.getVal());
                        //String recieverOfMyAck = pack.getSenderPort();

                        //pack.setSenderPort(myNode.getPort());
                        //pack.setpType(PacketType.INSERT_REPLICA_ACK);

                        //sendMessage(recieverOfMyAck, pack);
                        continue;
                        // send ack
                    }

                    int insertRepAckCount = 0;
                    if (pack.getpType() == PacketType.INSERT_REPLICA_ACK) {
                        System.out.println("Packet Type - " + pack.getpType());
                        insertRepAckCount++;
                        pack.setSenderPort(myNode.getPort());
                        pack.setpType(PacketType.INSERT_ACK);
                        if (insertRepAckCount==2) sendMessage(pack.getPacketInitiator(), pack);
                        //else if (insertRepAckCount==1 && )
                        continue;
                        // send ack *********************************************************8
                    }

                    if (pack.getpType() == PacketType.INSERT_ACK) {
                        isInsertAck = true;
                        //////////////// This has to be done on sender thread only... do this there only... check check.. check
                        // sendMessage(pack.getPacketInitiator(), pack);
                        continue;
                        // send ack
                    }

                    if (pack.getpType() == PacketType.DELETE_LOCAL_REQ) {
                        // delete files locally
                        deleteFilesLocally();
                        continue;
                    }
                    if (pack.getpType() == PacketType.DELETE_REQ || pack.getpType() == PacketType.DELETE_REPLICA_REQ) {
                        // delete a specific key
                        localData.remove(pack.getKey());
                        getContext().deleteFile(pack.getKey());
                        continue;
                    }

                    if (pack.getpType() == PacketType.QUERY_LOCAL_REQ) {
                        System.out.println("Received QUERY_LOCAL_REQ request from " + pack.getSenderPort() );
                        String response  = queryLocally("");
                        pack.setpType(PacketType.QUERY_LOCAL_ACK);
                        String sender = pack.getSenderPort();
                        pack.setReceiverPort(sender);
                        pack.setSenderPort(myNode.getPort());
                        pack.setKey(response);
                        System.out.println("Sending QUERY_LOCAL_ACK response "+ response+" to - "+pack.getSenderPort() + " from " + myNode.getPort());
                        sendMessage(sender, pack);
                        //System.out.println("CLIENT: Query ACK Packet Sent.");
                        continue;
                    }

                    if (pack.getpType() == PacketType.QUERY_LOCAL_ACK) {
                        String response = pack.getKey();
                        System.out.println("****QUERY_LOCAL_ACK received from********* "+pack.getSenderPort());
                        globalQueryResp += response;

                        numResponsesForGlobalQuery++;
                        if (numResponsesForGlobalQuery==5) {
                            allAckReceived=true;
                            numResponsesForGlobalQuery=0;
                        }
                        //System.out.println("Global Response-"+globalQueryResp);

                        continue;
                    }
                    if (pack.getpType() == PacketType.QUERY_REQ || pack.getpType() == PacketType.QUERY_REQ_REP1 || pack.getpType() == PacketType.QUERY_REQ_REP2) {
                        System.out.println("Received " +pack.getpType() +"request from " + pack.getSenderPort() + " for key " + pack.getKey());
                        String response = queryLocally(pack.getKey());
                        System.out.println("Sending QUERY_ACK response "+ response+" to - "+pack.getSenderPort() + " from " + myNode.getPort());
                        //if (pack.getpType() == PacketType.QUERY_REQ) pack.setpType(PacketType.QUERY_REQ_ACK);
                        //if (pack.getpType() == PacketType.QUERY_REQ_REP1) pack.setpType(PacketType.QUERY_REQ_REPL1_ACK);
                        //if (pack.getpType() == PacketType.QUERY_REQ_REP2) pack.setpType(PacketType.QUERY_REQ_REPL2_ACK);

                        String sender = pack.getSenderPort();
                        pack.setReceiverPort(sender);
                        pack.setSenderPort(myNode.getPort());
                        pack.setVal(response);

                        ObjectOutputStream out1 = new ObjectOutputStream(ss.getOutputStream());
                        //out1.flush();
                        System.out.println("Reading Packet");
                        out1.writeObject(pack);

                        System.out.println("Sending QUERY_ACK response "+ response+" to - "+pack.getSenderPort() + " from " + myNode.getPort());
                        //sendMessage(sender, pack);
                        //System.out.println("CLIENT: Query ACK Packet Sent.");
                        continue;
                    }
                    /*
                    if (pack.getpType() == PacketType.QUERY_REQ_ACK || pack.getpType() == PacketType.QUERY_REQ_REPL1_ACK || pack.getpType() == PacketType.QUERY_REQ_REPL2_ACK) {
                        System.out.println("***Received QUERY_REQ_ACK from " + pack.getSenderPort() + " with response " + pack.getKey()+"***");
                        //String response  = queryLocally(pack.getKey());
                        //pack.setpType(PacketType.QUERY_LOCAL_ACK);
                        //String sender = pack.getSenderPort();
                        //pack.setReceiverPort(sender);
                        //pack.setSenderPort(myNode.getPort());
                        String response = pack.getKey();
                        if (pack.getpType() == PacketType.QUERY_REQ_ACK) {
                            singleQueryResp = response;
                            isSingleQueryAnswered = true;
                        }
                        if (pack.getpType() == PacketType.QUERY_REQ_REPL1_ACK) {
                            singleQueryRespRepl1 = response;
                            isSingleQueryRepl1Answered = true;
                        }
                        if (pack.getpType() == PacketType.QUERY_REQ_REPL2_ACK) {
                            singleQueryRespRepl2 = response;
                            isSingleQueryRepl2Answered = true;
                        }

                        //System.out.println("Sending QUERY_ACK response "+ response+" to - "+pack.getSenderPort() + " from " + myNode.getPort());
                        //sendMessage(sender, pack);
                        //System.out.println("CLIENT: Query ACK Packet Sent.");
                        continue;
                    }
                    */
                    if (pack.getpType() == PacketType.REBALANCE_SELF_REQ) {
                        System.out.println ("***********REQUEST TYPE IS REBALANCE_SELF_REQ*************** from " + pack.getSenderPort() + " to " + myNode.getPort());
                        RingComparator keyComp = new RingComparator();
                        String response = queryLocally("");
                        System.out.println ("Full Query gives a response for REBALANCE_SELF_REQ is:"+response+ " from "+ pack.getSenderPort());
                        String finalResponse = "";
                        String[] allItems = response.split("&");
                        if (allItems != null && allItems.length>0)
                            for (String item: allItems) {
                                String[] key_val = item.split("@");
                                if (key_val!=null && key_val.length>0 && !key_val[0].equals("") && !key_val[1].equals("")) {
                                    try {
                                        int i;
                                        int predecessorId;
                                        String keySHA = genHash(key_val[0]);
                                        String portSHA = genHash(Integer.toString(Integer.valueOf(pack.getSenderPort())/2));

                                        for (i = 0; i < ringNodes.size(); i++) if (ringNodes.get(i).getPort().equals(pack.getSenderPort())) break;
                                        if (i == 0) predecessorId=4;
                                        else predecessorId = i-1;
                                        //System.out.println("Sender Node is " + pack.getSenderPort() + " and port id is "+i);

/*                                        if ((keySHA.equals(sNode.getPortSHA())) ||
                                                (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && sNode.isFirstNode())||
                                                (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1 && sNode.isFirstNode()) ||
                                                (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorPortSHA() )==1)))
*/
                                        System.out.println("Sender Port is "+pack.getSenderPort()+" and predecessor port is "+Integer.valueOf(remotePort[predecessorId])/2);
                                        System.out.println("Port Hash is "+portSHA + " and ringNodes.get(0) is "+ ringNodes.get(predecessorId).getPort());
                                        String firstNode = ringNodes.get(0).getPort();
                                        String firstNodeSHA = genHash(Integer.toString(Integer.valueOf(firstNode)/2));
                                        String predecessorSHA = genHash(Integer.toString(Integer.valueOf(ringNodes.get(predecessorId).getPort())/2));

                                        System.out.println("Predecessor ID is-"+predecessorId+" and Main Node ID is "+i+"First Node SHA is "+
                                                firstNodeSHA + " and predecessor SHA is "+ predecessorSHA + " and main port SHA is "+ portSHA +
                                                " and keySHA is " + keySHA);


                                        System.out.println("Various comparison params are keySHA.equals(portSHA)-"+
                                                keySHA.equals(portSHA)+" \t keyComp.compare(keySHA, portSHA)-"+keyComp.compare(keySHA, portSHA) +
                                                " \t portSHA.equals(firstNodeSHA))-"+ portSHA.equals(firstNodeSHA) +
                                                " \t keyComp.compare(keySHA, predecessorSHA)-"+keyComp.compare(keySHA, predecessorSHA) +
                                                " \t portSHA.equals(firstNodeSHA))-"+ portSHA.equals(firstNodeSHA) +
                                                " \t keyComp.compare(keySHA, portSHA)-"+keyComp.compare(keySHA, portSHA) +
                                                " \t keyComp.compare(keySHA, predecessorSHA)==1)-"+keyComp.compare(keySHA, predecessorSHA) );

                                        if (keySHA.equals(portSHA) ||
                                                (keyComp.compare(keySHA, portSHA) == -1 && portSHA.equals(firstNodeSHA)) ||
                                                (keyComp.compare(keySHA, predecessorSHA) == 1 && portSHA.equals(firstNodeSHA)) ||
                                                (keyComp.compare(keySHA, portSHA) == -1 && keyComp.compare(keySHA, predecessorSHA)==1)) {
                                            finalResponse += key_val[0] + "@" + key_val[1] + "&";
                                        }
                                    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
                                }
                            }
                        System.out.println ("Final Response for REBALANCE_SELF_REQ is:"+finalResponse);
                        pack.setpType(PacketType.REBALANCE_SELF_ACK);
                        pack.setKey(finalResponse);
                        sendMessage(pack.getSenderPort(), pack);
                        continue;
                    }

                    if (pack.getpType() == PacketType.REBALANCE_OTH_REQ) {
                        System.out.println ("***********REQUEST TYPE IS REBALANCE_OTH_REQ*************** from " + pack.getSenderPort() + " to " + myNode.getPort());
                        RingComparator keyComp = new RingComparator();
                        String response = queryLocally("");
                        System.out.println ("Full Query gives a response for REBALANCE_SELF_REQ is:"+response+ " from "+ pack.getSenderPort());
                        String finalResponse = "";
                        String[] allItems = response.split("&");
                        if (allItems != null && allItems.length>0)
                            for (String item: allItems) {
                                String[] key_val = item.split("@");
                                if (key_val!=null && key_val.length>0 && !key_val[0].equals("") && !key_val[1].equals("")) {
                                    try {
                                        int i;
                                        int predecessorId;
                                        String keySHA = genHash(key_val[0]);
                                        String portSHA = genHash(Integer.toString(Integer.valueOf(myNode.getPort())/2));

                                        for (i = 0; i < ringNodes.size(); i++) if (ringNodes.get(i).getPort().equals(myNode.getPort())) break;
                                        if (i == 0) predecessorId=4;
                                        else predecessorId = i-1;
                                        //System.out.println("Sender Node is " + pack.getSenderPort() + " and port id is "+i);

/*                                        if ((keySHA.equals(sNode.getPortSHA())) ||
                                                (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && sNode.isFirstNode())||
                                                (keyComp.compare(keySHA, sNode.getPredecessorPortSHA()) == 1 && sNode.isFirstNode()) ||
                                                (keyComp.compare(keySHA, sNode.getPortSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorPortSHA() )==1)))
*/
                                        //System.out.println("Sender Port is "+pack.getSenderPort()+" and predecessor port is "+Integer.valueOf(remotePort[predecessorId])/2);
                                        //System.out.println("Port Hash is "+portSHA + " and ringNodes.get(0) is "+ ringNodes.get(predecessorId).getPort());
                                        String firstNode = ringNodes.get(0).getPort();
                                        String firstNodeSHA = genHash(Integer.toString(Integer.valueOf(firstNode)/2));
                                        String predecessorSHA = genHash(Integer.toString(Integer.valueOf(ringNodes.get(predecessorId).getPort())/2));

                                        System.out.println("Predecessor ID is-"+predecessorId+" and Main Node ID is "+i+"First Node SHA is "+
                                                firstNodeSHA + " and predecessor SHA is "+ predecessorSHA + " and main port SHA is "+ portSHA +
                                                " and keySHA is " + keySHA);

                                        /*
                                        System.out.println("Various comparison params are keySHA.equals(portSHA)-"+
                                                keySHA.equals(portSHA)+" \t keyComp.compare(keySHA, portSHA)-"+keyComp.compare(keySHA, portSHA) +
                                                " \t portSHA.equals(firstNodeSHA))-"+ portSHA.equals(firstNodeSHA) +
                                                " \t keyComp.compare(keySHA, predecessorSHA)-"+keyComp.compare(keySHA, predecessorSHA) +
                                                " \t portSHA.equals(firstNodeSHA))-"+ portSHA.equals(firstNodeSHA) +
                                                " \t keyComp.compare(keySHA, portSHA)-"+keyComp.compare(keySHA, portSHA) +
                                                " \t keyComp.compare(keySHA, predecessorSHA)==1)-"+keyComp.compare(keySHA, predecessorSHA) );
                                        */
                                        if (keySHA.equals(portSHA) ||
                                                (keyComp.compare(keySHA, portSHA) == -1 && portSHA.equals(firstNodeSHA)) ||
                                                (keyComp.compare(keySHA, predecessorSHA) == 1 && portSHA.equals(firstNodeSHA)) ||
                                                (keyComp.compare(keySHA, portSHA) == -1 && keyComp.compare(keySHA, predecessorSHA)==1)) {
                                            finalResponse += key_val[0] + "@" + key_val[1] + "&";
                                        }
                                    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
                                }
                            }
                        System.out.println ("Final Response for REBALANCE_OTH_REQ is:"+finalResponse);
                        pack.setpType(PacketType.REBALANCE_OTH_ACK);
                        pack.setKey(finalResponse);
                        sendMessage(pack.getSenderPort(), pack);
                        continue;
                    }

                    if (pack.getpType() == PacketType.REBALANCE_SELF_ACK) {
                        System.out.println("Rebalance self ack received...");
                        System.out.println("Received Pack is " + pack.getKey());
                        String data = pack.getKey();
                        System.out.println ("REBALANCE_SELF_REQ is:"+data+ " from "+ pack.getSenderPort());
                        String[] allItems = data.split("&");
                        if (allItems != null && allItems.length>0)
                            for (String item: allItems) {
                                String[] key_val = item.split("@");
                                if (key_val != null && key_val.length > 0 && !key_val[0].equals("") && !key_val[1].equals("")) {
                                    insertLocally(key_val[0], key_val[1]);
                                    System.out.println("Storing data - " + key_val[0] + "\t" + key_val[1]);
                                }
                            }
                        continue;
                    }

                    if (pack.getpType() == PacketType.REBALANCE_OTH_ACK) {
                        System.out.println("Rebalance other ack received...");
                        System.out.println("Received Pack is " + pack.getKey());
                        String data = pack.getKey();
                        System.out.println ("REBALANCE_OTH_ACK is:"+data+ " from "+ pack.getSenderPort());
                        String[] allItems = data.split("&");
                        if (allItems != null && allItems.length>0)
                            for (String item: allItems) {
                                String[] key_val = item.split("@");
                                if (key_val != null && key_val.length > 0 && !key_val[0].equals("") && !key_val[1].equals("")) {
                                    System.out.println("Storing data - " + key_val[0] + "\t" + key_val[1]);
                                    insertLocally(key_val[0], key_val[1]);
                                }
                            }
                        continue;
                    }
                }
            } catch (Exception e) { e.printStackTrace(); }
            return null;
        }
    }

    protected Boolean isKeyBelongsToPort (String key, String port) {
        try {
            String keySHA = genHash(key);
            String portSHA = genHash(port);
            RingComparator rComp = new RingComparator();
            int retVal = rComp.compare(keySHA, portSHA);
            if (retVal<0) return true;
            else return false;
        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
        return false;
    }

    protected void sendMessage (String senderPort, Packet pack) {
        Socket socket;
        //System.out.println("****Sending response to port-"+senderPort+"****");
        try {
            socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.valueOf(senderPort));

            ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
            ost.writeObject(pack);
            //ost.flush();
            //ost.close();
            //socket.close();
        } catch (UnknownHostException e) {
            System.out.println("ClientTask UnknownHostException");
        } catch (IOException e) {
            System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
        }
    }

    protected Socket sendMessage (String senderPort, Packet pack, Boolean ret) {
        Socket socket;
        //System.out.println("****Sending response to port-"+senderPort+"****");
        try {
            socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.valueOf(senderPort));
            socket.setSoTimeout(8000);
            ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
            ost.writeObject(pack);
            return socket;
            //ost.flush();
            //ost.close();
            //socket.close();
        } catch (UnknownHostException e) {
            System.out.println("ClientTask UnknownHostException");
        } catch (IOException e) {
            System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
        }
        return null;
    }

    private class ClientTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... msgs) {
            if (msgs[0].equals("INSERT_REQ") || msgs[0].equals("INSERT_REPLICA_REQ") ) {
                Packet pack = new Packet();
                if (msgs[0].equals("INSERT_REQ")) pack.setpType(PacketType.INSERT_REQ);
                else if (msgs[0].equals("INSERT_REPLICA_REQ")) pack.setpType(PacketType.INSERT_REPLICA_REQ);
                pack.setSenderPort(myNode.getPort());
                pack.setReceiverPort(msgs[1]);
                pack.setKey(msgs[2]);
                pack.setVal(msgs[3]);

                sendMessage(msgs[1], pack);
                System.out.println("CLIENT: First Packet Sent.");
            }
            else if (msgs[0].equals("DELETE_LOCAL_REQ")) {
                Packet pack = new Packet(PacketType.DELETE_LOCAL_REQ);
                sendMessage(msgs[1], pack);
            }

            else if (msgs[0].equals("DELETE_REQ") || msgs[0].equals("DELETE_REPLICA_REQ") ) {
                Packet pack = new Packet();
                if (msgs[0].equals("DELETE_REQ")) pack.setpType(PacketType.DELETE_REQ);
                else if (msgs[0].equals("DELETE_REPLICA_REQ")) pack.setpType(PacketType.DELETE_REPLICA_REQ);
                pack.setSenderPort(myNode.getPort());
                pack.setReceiverPort(msgs[1]);
                pack.setKey(msgs[2]);

                sendMessage(msgs[1], pack);
                System.out.println("CLIENT: Delete Packet Sent.");
            }

            else if (msgs[0].equals("QUERY_REQ")) {
                System.out.println("CLIENT: Quering for key " + msgs[2] + " by Sending request to " + msgs[1]);
                //new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "QUERY_REQ", sNode.getPort(), key);

                Packet pack = new Packet(PacketType.QUERY_REQ);
                pack.setReceiverPort(msgs[1]);
                pack.setSenderPort(myNode.getPort());
                pack.setKey(msgs[2]);
                System.out.println("CLIENT: Query Packet Sending 2.");

                sendMessage(msgs[1], pack);
                System.out.println("CLIENT: Query Packet Sent.");
            }
            else if (msgs[0].equals("REBALANCE_REQ")) {
                rebalanceData();
            }
            /*
            else if (msgs[0].equals("QUERY_LOCAL_ACK")) {

                Packet pack = new Packet(PacketType.QUERY_LOCAL_ACK);
                pack.setReceiverPort(msgs[1]);
                pack.setSenderPort(myNode.getPort());
                pack.setKey(msgs[2]);

                sendMessage(msgs[1], pack);
                System.out.println("CLIENT: Query ACK Packet Sent.");
            }
            */
            return null;
        }
    }
}