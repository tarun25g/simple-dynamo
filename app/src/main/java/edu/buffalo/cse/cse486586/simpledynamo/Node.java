package edu.buffalo.cse.cse486586.simpledynamo;

/**
 * Created by tarun on 4/17/15.
 */
public class Node {
    private Boolean isFirstNode;

    private String port;
    private String portSHA;

    private String successorPort;
    private String successorPortSHA;

    private String predecessorPort;
    private String predecessorPortSHA;

    public String getPort() {
        return port;
    }
    public void setPort(String port) {
        this.port = port;
    }

    public String getPortSHA() {
        return portSHA;
    }
    public void setPortSHA(String portSHA) {
        this.portSHA = portSHA;
    }

    public String getPredecessorPort() {
        return predecessorPort;
    }
    public void setPredecessorPort(String predecessorPort) {
        this.predecessorPort = predecessorPort;
    }

    public String getPredecessorPortSHA() {
        return predecessorPortSHA;
    }
    public void setPredecessorPortSHA(String predecessorPortSHA) {
        this.predecessorPortSHA = predecessorPortSHA;
    }

    public void setSuccessorPort(String successorPort) {
        this.successorPort = successorPort;
    }
    public String getSuccessorPort() {
        return successorPort;
    }

    public String getSuccessorPortSHA() {
        return successorPortSHA;
    }
    public void setSuccessorPortSHA(String successorPortSHA) {
        this.successorPortSHA = successorPortSHA;
    }

    public void setIsFirstNode(Boolean isFirstNode) {
        this.isFirstNode = isFirstNode;
    }
    public Boolean isFirstNode() {
        return isFirstNode;
    }
}
