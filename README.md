Amazon's Mini Dynamo - A scalable distributed key value store.
- Simplified version of Dynamo (Key-Value storage) which is implemented on Android devices has following features :
	1. Consistent Hashing (SHA-1) based ID space partitioning/re-partitioning of nodes and keys.
	2. Node joins/ failures management.
	3. Quorum based replication (3 replicas here - default).
	4. Recovery from replicated storage after failure.

Each node can figure out which node will contain a specific key based on Consistent Hashed Partitioning. So if an insert/ query request comes, the query will be directed to that node and if the node doesn't send ACK within heartbeat timeout, it is assumed to be failed, query is sent to next node which has replica and finally if ACK is received, it is send back to client.


Here, Content Provider is FileSystem where File Name is the key and File Contents are the Value corresponding to that key.

As this is a prototype, all focus is on correctness rather than performance

More About Amazon Dynamo - http://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf